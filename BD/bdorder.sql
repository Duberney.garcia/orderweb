-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.44 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para bdorder
CREATE DATABASE IF NOT EXISTS `bdorder` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `bdorder`;

-- Volcando estructura para tabla bdorder.activity
CREATE TABLE IF NOT EXISTS `activity` (
  `id_activity` int(11) NOT NULL COMMENT 'id actividad',
  `description` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripción de la actividad',
  `hours` int(11) NOT NULL COMMENT 'horas estimadas de duración',
  `date` date NOT NULL COMMENT 'fecha',
  `id_technician` bigint(20) DEFAULT NULL COMMENT 'id técnico que hace la actividad',
  `id_type` int(11) DEFAULT NULL COMMENT 'id tipo de actividad',
  PRIMARY KEY (`id_activity`),
  KEY `id_technician` (`id_technician`),
  KEY `id_type` (`id_type`),
  CONSTRAINT `activity_ibfk_1` FOREIGN KEY (`id_technician`) REFERENCES `technician` (`document`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `activity_ibfk_2` FOREIGN KEY (`id_type`) REFERENCES `type_activity` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de actividades';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bdorder.causal
CREATE TABLE IF NOT EXISTS `causal` (
  `id_causal` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id causal',
  `description` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripción de la causal',
  PRIMARY KEY (`id_causal`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla causales de la actividad';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bdorder.observation
CREATE TABLE IF NOT EXISTS `observation` (
  `id_observation` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id observacion',
  `description` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripción de la observación',
  PRIMARY KEY (`id_observation`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla observaciones no cumplimiento';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bdorder.order
CREATE TABLE IF NOT EXISTS `order` (
  `id_order` int(11) NOT NULL COMMENT 'id orden',
  `legalization_date` date NOT NULL COMMENT 'fecha legalización',
  `address` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'dirección',
  `city` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'ciudad',
  `state` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'departamento o estado',
  `id_observation` int(11) DEFAULT NULL COMMENT 'id observacion',
  `id_causal` int(11) DEFAULT NULL COMMENT 'id causal',
  PRIMARY KEY (`id_order`),
  KEY `id_causal` (`id_causal`),
  KEY `id_observation` (`id_observation`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`id_causal`) REFERENCES `causal` (`id_causal`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_ibfk_2` FOREIGN KEY (`id_observation`) REFERENCES `observation` (`id_observation`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de orden de trabajo';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bdorder.order_activity
CREATE TABLE IF NOT EXISTS `order_activity` (
  `id_order` int(11) NOT NULL,
  `id_activity` int(11) NOT NULL,
  `serial` int(11) NOT NULL,
  PRIMARY KEY (`id_order`,`id_activity`),
  KEY `id_activity` (`id_activity`),
  CONSTRAINT `order_activity_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_activity_ibfk_2` FOREIGN KEY (`id_activity`) REFERENCES `activity` (`id_activity`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bdorder.technician
CREATE TABLE IF NOT EXISTS `technician` (
  `document` bigint(20) NOT NULL COMMENT 'cedula del técnico',
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'nombre completo',
  `especiality` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'especialidad del técnico',
  `phone` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'teléfono',
  PRIMARY KEY (`document`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de técnicos';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bdorder.type_activity
CREATE TABLE IF NOT EXISTS `type_activity` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id tipo de actividad',
  `description` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripción',
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de tipos de actividad';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla bdorder.user
CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'nombre de usuario',
  `password` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'contraseña del usuario',
  `role` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'rol o tipo de usuario. SUPERVISOR, ADMINISTRADOR',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de usuarios';

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
